<?php

namespace App\Contracts;

/**
 * Interface OrderContract
 * @package App\Contracts
 */

interface OrderContract
{
    /**
     * @param $params
     * @return mixed
     */

    public function storeOrderDetails($params);

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return mixed
     */

    public function listOrders(string $order = 'id', string $sort = 'desc', array $columns = ['*']);

    /**
     * @param $orderNumber
     * @return mixed
     */

    public function findOrderByNumber($orderNumber);
}
